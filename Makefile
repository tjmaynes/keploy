install:
	cargo install --path .

test: install
	cargo $@

build:
	cargo $@

start:
	cargo run

deploy: test
