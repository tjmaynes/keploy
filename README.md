# Keploy
> An opinionated tool for generating Istio + Kubernetes based deployment resources for backend services from a simple json file.

## Requirements

- [Rust](https://rustup.rs/)

## Usage

First, create a `.keployrc.json` file:
```json
{
  "name": "your-app-name",
  "image_name": "ngnix:alpine",
  "port": 8080
}
```

To generate your app's Kubernetes resources, run the following command:
```bash
keploy generate -f .keployrc.json 
```

## Development

To install project dependencies, run the following commmand:
```bash
make install
```

To start the project, run the following command:
```bash
make start
```

To run project tests, run the following command:
```bash
make test
```
